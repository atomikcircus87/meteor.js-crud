Router.route('/', function () {
  this.render('home');
},{
	name: 'home'
});

Router.route('/all', function () {
  this.render('all');
},{
	name: 'all'
});

Router.route('delete/:_id', function(){
	var params = this.params;
	var autoDelete = Autos.findOne({_id:params._id});

	Meteor.call('deleteAuto', {_id:autoDelete._id}, function(error, result){});	
	this.redirect('/all');
});

Router.route('read/:_id',{
	template: 'read',
    data: function(){
        var autoId = this.params._id;
        return Autos.findOne({ _id: autoId });
    }
});