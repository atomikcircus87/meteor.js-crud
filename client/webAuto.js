Meteor.subscribe('autos');


Template.form.rendered = function () {
  $(".form").hide();
};

Template.formEdit.rendered = function () {
  $(".formEdit").hide();
};

Template.formInscription.rendered = function () {
  $(".formInscri").hide();
};

Template.login.rendered = function () {
  $(".login").hide();
};

Template.container.helpers({
	autos: function(){
		return Autos.find({});
	}
});

Template.containerRead.helpers({
	autom: function(){
		return Autos.find({});
	},
});

Template.containerRead.events({
	'click .btn': function (event){
		$(".formEdit").toggle();
	}

});

Template.form.events({
	'click .btn': function (event) {
		event.preventDefault();
		var marque = $('#marque').val();
		var type = $('#type').val();
		var comment = $('#comment').val();

		Meteor.call('insertAuto', {marque: marque, type: type, comment:comment}, function (error, result) {});
		$(".form").hide();
	},
});

Template.formEdit.events({
	'click .btn': function (event) {
		event.preventDefault();
		var marque = $('#marque').val();
		var type = $('#type').val();
		var comment = $('#comment').val();

      Autos.update(this._id, {
        $set: {marque: marque, type: type, comment:comment}
      });
	},
});


Template.navAll.events({
  "click .toggleForm":function(event, template){
    $(".form").toggle();
	$(".formInscri").hide();
	},
  "click .toggleFormInscri":function(event, template){
    $(".formInscri").toggle();
    $(".form").hide();
    $(".login").hide();
  },
  "click .toggleLogin":function(event, template){
    $(".login").toggle();
    $(".formInscri").hide();
    $(".form").hide();
  },
  "click .logout": function(event, template) {
	Meteor.logout();
},
});

Template.formInscription.events({
    "submit form": function(e, template) {
		e.preventDefault();

		var username = $('#username').val();
		var email = $('#email').val();
		var password = $('#password').val();

		var user = {
			username: username,
			email: email,
			password: password,
		};

		Accounts.createUser(user, function(err) { 
			if (err) {
				alert(err.reason)
			} else {
				Router.go('all'); 
			}
		});
	}
});

Template.login.events({
    "submit form": function(event, template) {
		event.preventDefault();

		var user = $("#username").val();
		var password = $("#password").val();

		Meteor.loginWithPassword(user,password,function(err) {
				if (err) {
					alert(err.reason + "erreur")
				}
			});
		$(".login").toggle();
	}
});