Autos = new Mongo.Collection("autos");

Meteor.methods({
	insertAuto: function(auto){
		NonEmptyString = Match.Where(function (x) {
			check(x, String);
			return x.length > 0;
		}); 
		check(auto.marque, NonEmptyString);
		check(auto.type, NonEmptyString);
		check(auto.comment, NonEmptyString);

		newAuto = Autos.insert({marque: auto.marque, type:auto.type, comment:auto.comment, createdAt : Date.now()}, function(error, result){});

		return newAuto;
	},

	deleteAuto: function(auto){

		Autos.remove({_id: auto._id,}, function(error, result){});
	}
})